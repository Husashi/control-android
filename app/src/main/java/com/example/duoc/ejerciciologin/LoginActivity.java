package com.example.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.duoc.ejerciciologin.bd.AlmacenUsuarios;
import com.example.duoc.ejerciciologin.entidad.FormularioRegistro;

import java.util.List;


public class LoginActivity extends AppCompatActivity {
    private EditText etUsuario, etPass;
    private Button btnLogin, btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginn);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etPass=(EditText) findViewById(R.id.etPass);
        btnLogin=(Button)findViewById(R.id.btnLogin);
        btnRegistrar=(Button)findViewById(R.id.btnRegistro);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<FormularioRegistro> l= AlmacenUsuarios.getFormularios();
                for (FormularioRegistro f : l) {
                    if(etUsuario.getText().toString().equals(f.getUsuario().toString()) && etPass.getText().toString().equals(f.getClave().toString()))
                    {
                        Intent i=new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(i);
                    }
                    else{
                        Toast.makeText(LoginActivity.this, "Ingrese una cuenta valida", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(LoginActivity.this,RegistroActivity.class);
                startActivity(home);
            }
        });
    }
}
