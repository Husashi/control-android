package com.example.duoc.ejerciciologin.bd;

import com.example.duoc.ejerciciologin.entidad.FormularioRegistro;

import java.util.ArrayList;

/**
 * Created by DUOC on 25-03-2017.
 */

public class AlmacenUsuarios {
    public static ArrayList<FormularioRegistro> formularios = new ArrayList<>();

    public static void agregarFormulario(FormularioRegistro formulario){formularios.add(formulario);}

    public static ArrayList<FormularioRegistro> getFormularios(){
        return formularios;
    }
}
