package com.example.duoc.ejerciciologin;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.ejerciciologin.bd.AlmacenUsuarios;
import com.example.duoc.ejerciciologin.entidad.FormularioRegistro;

public class RegistroActivity extends AppCompatActivity {
private EditText etUsuario, etNombre, etApellidos, etRut, etClave, etRClave;
    private Button btnRegistro, etFechaNacimiento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario= (EditText) findViewById(R.id.etUsuario);
        etNombre=(EditText)findViewById(R.id.etNombre);
        etApellidos=(EditText)findViewById(R.id.etApellidos);
        etRut=(EditText)findViewById(R.id.etRut);
        etFechaNacimiento=(Button)findViewById(R.id.etFechaNacimiento);
        etClave=(EditText)findViewById(R.id.etClave);
        etRClave=(EditText)findViewById(R.id.etRClave);
        btnRegistro=(Button)findViewById(R.id.btnRegistro);
        etFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarRegistro();
            }
            public boolean validarRut(String rut) {

                boolean validacion = false;
                try {
                    rut =  rut.toUpperCase();
                    rut = rut.replace(".", "");
                    rut = rut.replace("-", "");
                    int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

                    char dv = rut.charAt(rut.length() - 1);

                    int m = 0, s = 1;
                    for (; rutAux != 0; rutAux /= 10) {
                        s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                    }
                    if (dv == (char) (s != 0 ? s + 47 : 75)) {
                        validacion = true;
                    }

                } catch (java.lang.NumberFormatException e) {
                } catch (Exception e) {
                }
                return validacion;
            }
            private void guardarRegistro() {
                String mensajeError = "";

                if(etUsuario.getText().toString().length()<1){
                    mensajeError += "Ingrese Usuario \n";
                }
                if (etNombre.getText().toString().length() < 1) {
                    mensajeError += "Ingrese Nombre \n";
                }
                if (etApellidos.getText().toString().length() < 1) {
                    mensajeError += "Ingrese apellidos \n";
                }
                if (!validarRut(etRut.getText().toString())) {
                    mensajeError += "Ingrese rut valido \n";
                }
                if(etClave.getText().toString().length()<1){
                    mensajeError += "Ingrese clave valida \n";
                }
                if(!etRClave.getText().toString().equals(etClave.getText().toString()))
                {
                    mensajeError += "Ingrese las claves no coinsiden \n";

                }

                if(mensajeError.equals(""))
                {
                        FormularioRegistro nuevoForm = new FormularioRegistro();
                        nuevoForm.setUsuario(etUsuario.getText().toString());
                        nuevoForm.setNombre(etNombre.getText().toString());
                        nuevoForm.setApellidos(etApellidos.getText().toString());
                        nuevoForm.setRut(etRut.getText().toString());
                        nuevoForm.setFechaNacimiento(etFechaNacimiento.getText().toString());
                        nuevoForm.setClave(etClave.getText().toString());
                        AlmacenUsuarios.agregarFormulario(nuevoForm);
                        Toast.makeText(RegistroActivity.this, "Formulario guardado correctamente", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(RegistroActivity.this,LoginActivity.class);
                    startActivity(i);

                }
                else{
                    Toast.makeText(RegistroActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }





    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFechaNacimiento.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }
}
