package com.example.duoc.ejerciciologin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by DUOC on 25-03-2017.
 */

public class HomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
    }
}
